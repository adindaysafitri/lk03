abstract class Kalkulator { //sebagai superclass
    double operan1, operan2;
    //Do your magic here...
    abstract public double hitung();

    public void setOperan(double operand1, double operand2) {
        this.operan1 = operand1;
        this.operan2 = operand2;
    }
}